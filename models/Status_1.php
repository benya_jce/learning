<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "status_1".
 *
 * @property integer $id
 * @property string $name
 */
class Status_1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
		public static function getStatuses()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'name');
		return $allStatusesArray;						
	}
}
