<?php

use yii\db\Migration;

class m160725_132334_init2_status_table extends Migration
{
    public function up()
    {
		$this->createTable(
            'status_1',
            [
                'id' => 'pk',
                'name' => 'string',	
				
							
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
        echo "m160725_132334_init2_status_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
