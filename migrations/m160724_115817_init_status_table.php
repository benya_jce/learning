<?php

use yii\db\Migration;

class m160724_115817_init_status_table extends Migration
{
    public function up()
    {
        $this->createTable(
            'status',
            [
                'id' => 'pk',
                'name' => 'string',	
				'created_at'=>'integer',
				'updated_at'=>'integer',
				'created_by'=>'integer',
				'updated_by'=>'integer'				
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('status');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}	
