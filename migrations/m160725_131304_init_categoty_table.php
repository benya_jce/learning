<?php

use yii\db\Migration;

class m160725_131304_init_categoty_table extends Migration
{
    public function up()
   {
        $this->createTable(
            'category',
            [
                'id' => 'pk',
                'name' => 'string',	
						
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('category');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
