<?php

use yii\db\Migration;

class m160725_130423_alter_user_table extends Migration
{
    public function up()
    {
		$this->addColumn('user','CategoryId','integer');
    }

    public function down()
    {
       $this->dropColumn('user','CategoryId');
      
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
