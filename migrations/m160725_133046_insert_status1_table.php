<?php

use yii\db\Migration;

class m160725_133046_insert_status1_table extends Migration
{
    public function up()
     {
        $this->insert('status_1', [
            'name' => 'published',
        ]);
        $this->insert('status_1', [
            'name' => 'waiting',
        ]);	
        $this->insert('status_1', [
            'name' => 'rejected',
        ]);		
    }

    public function down()
    {
		$this->delete('status_1', ['name' => 'published']);
		$this->delete('status_1', ['name' => 'waiting']);
		$this->delete('status_1', ['name' => 'rejected']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
