<?php

use yii\db\Migration;

class m160725_132714_alter2_category_table extends Migration
{
    public function up()
    {
        $this->insert('category', [
            'name' => 'handcraft',
        ]);
        $this->insert('category', [
            'name' => 'sport',
        ]);	
        $this->insert('category', [
            'name' => 'games',
        ]);		
    }

    public function down()
    {
		$this->delete('category', ['name' => 'handcraft']);
		$this->delete('category', ['name' => 'sport']);
		$this->delete('category', ['name' => 'games']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
