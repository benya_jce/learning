<?php

use yii\db\Migration;

class m160725_131614_init_activity_table extends Migration
{
    public function up()
    {
        $this->createTable(
            'activity',
            [
                'id' => 'pk',
                'title' => 'string',	
				'categoryId'=>'integer',
				'statusId'=>'integer',
							
            ],
            'ENGINE=InnoDB'
        );
    }

    public function down()
    {
         $this->dropTable('activity');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
