<?php

use yii\db\Migration;

class m160724_120632_insert_status_table extends Migration
{
    public function up()
    {
        $this->insert('status', [
            'name' => 'Lead',
        ]);
        $this->insert('status', [
            'name' => 'Qualified lead',
        ]);	
        $this->insert('status', [
            'name' => 'Customer',
        ]);		
    }

    public function down()
    {
		$this->delete('status', ['name' => 'Lead']);
		$this->delete('status', ['name' => 'Qualified lead']);
		$this->delete('status', ['name' => 'Customer']);
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
